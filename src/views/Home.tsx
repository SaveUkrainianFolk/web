import React, { useEffect, useState } from 'react';
import { addTwitterWidgetScript, isMobile } from '../utils';
import {
  Header,
  Footer,
  Mission,
  Banner,
  Mint,
  Audience,
  Team,
  FAQTwitter,
} from '../components';

export const Home = () => {
  const [isPlayerOpen, setIsPlayerOpen] = useState(false);

  const handlePlayerOpen = () => {
    setIsPlayerOpen(true);
  };

  useEffect(() => {
    addTwitterWidgetScript();
  }, []);

  return (
    <div
      className="Home"
      style={{ paddingBottom: isPlayerOpen && isMobile ? '100px' : 0 }}
    >
      <Header handlePlayerOpen={handlePlayerOpen} />
      <main className="main">
        <Mission />
        <Banner />
        <Mint />
        <Audience />
        <Team />
        <FAQTwitter />
      </main>
      <Footer />
    </div>
  );
};
