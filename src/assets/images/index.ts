import Logo from './logo.png';
import HeaderBackground from './header-background.jpg';
import LoveFromUa from './love-from-ua.png';
import NFT from './nft.jpg';
import MangataaqiuveraB from './mangataaqiuvera-b.png';
import MangataaqiuveraW from './mangataaqiuvera-w.png';
import NationalBorder from './national-border.png';
import Opensea from './opensea.svg';
import MissionVideoPreview from './mission-video-preview.png';
import PlayButton from './play-button.png';

import Max from './team/Max.jpg';
import Petro from './team/Petro.jpg';
import Marta from './team/Marta.jpg';
import Vlad from './team/Vlad.jpg';
import Dima from './team/Dima.jpg';
import Dasha from './team/Dasha.jpg';
import Elina from './team/Elina.jpg';

export {
  Logo,
  HeaderBackground,
  LoveFromUa,
  NFT,
  MangataaqiuveraB,
  MangataaqiuveraW,
  NationalBorder,
  Opensea,
  MissionVideoPreview,
  PlayButton,
  Max,
  Petro,
  Marta,
  Vlad,
  Dima,
  Dasha,
  Elina,
};
