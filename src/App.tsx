import React from 'react';
import { Home } from './views';
import { QueryClient, QueryClientProvider } from 'react-query';
import { WagmiConfig, createClient, chain, configureChains } from 'wagmi';
import { alchemyProvider } from 'wagmi/providers/alchemy';
import { publicProvider } from 'wagmi/providers/public';
import '@rainbow-me/rainbowkit/styles.css';
import { alchemyApiKey } from './constants';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {
  connectorsForWallets,
  wallet,
  RainbowKitProvider,
  midnightTheme,
} from '@rainbow-me/rainbowkit';

const { chains, provider } = configureChains(
  [chain.mainnet],
  [alchemyProvider({ alchemyId: alchemyApiKey }), publicProvider()]
);

const connectors = connectorsForWallets([
  {
    groupName: 'Recommended',
    wallets: [
      // wallet.rainbow({ chains }),
      wallet.metaMask({ chains }),
    ],
  },
  // {
  //     groupName: 'Others',
  //     wallets: [
  //         wallet.walletConnect({ chains }),
  //         wallet.coinbase({ chains, appName: '#SaveUkrainianFolk' }),
  //         wallet.trust({ chains }),
  //         wallet.imToken({ chains }),
  //     ],
  // },
]);

const wagmiClient = createClient({
  autoConnect: true,
  connectors,
  provider,
});

const queryClient = new QueryClient();

const App = () => (
  <WagmiConfig client={wagmiClient}>
    <RainbowKitProvider
      chains={chains}
      theme={midnightTheme({
        accentColor: 'rgb(247, 38, 38)',
      })}
    >
      <QueryClientProvider client={queryClient}>
        <ToastContainer />
        <Home />
      </QueryClientProvider>
    </RainbowKitProvider>
  </WagmiConfig>
);

export default App;
