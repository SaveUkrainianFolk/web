import { isMobile } from '../isMobile';

export const addNftImageAnimation = () => {
  if (isMobile) return;
  const ANGLE_COMPENSATION = 50;
  document
    .getElementsByTagName('header')[0]
    .addEventListener('mousemove', (event) => {
      const xOffset = window.innerWidth / 2 - event.clientX;
      const yOffset = window.innerHeight / 2 - event.clientY;
      const xRotationAngle = yOffset / ANGLE_COMPENSATION;
      const yRotationAngle = xOffset / ANGLE_COMPENSATION;
      document.getElementById('nft')!.style.transform =
        'rotateX(' + xRotationAngle + 'deg) rotateY(' + yRotationAngle + 'deg)';
    });
};
