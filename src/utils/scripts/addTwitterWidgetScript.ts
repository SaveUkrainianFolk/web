export const addTwitterWidgetScript = () => {
    const twitterWidgetScript = document.createElement('script');
    twitterWidgetScript.setAttribute('src', 'https://platform.twitter.com/widgets.js');
    twitterWidgetScript.setAttribute("async", "");
    document.head.appendChild(twitterWidgetScript);
};
