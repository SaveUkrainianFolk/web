import React from 'react';
import { FAQ, FAQProps } from './';
import {
  FAQ_A1,
  FAQ_A2,
  FAQ_A3,
  FAQ_A4,
  FAQ_A5,
  FAQ_Q1,
  FAQ_Q2,
  FAQ_Q3,
  FAQ_Q4,
  FAQ_Q5,
  links,
  MORE_IN_OUR_TWITTER_BUTTON_TEXT,
} from '../../../constants';
import { FaTwitter } from 'react-icons/fa';
import { isMobile } from '../../../utils';

const FAQs: FAQProps[] = [
  {
    question: FAQ_Q1.en,
    answer: FAQ_A1.en,
    open: true,
  },
  {
    question: FAQ_Q2.en,
    answer: FAQ_A2.en,
  },
  {
    question: FAQ_Q3.en,
    answer: FAQ_A3.en,
  },
  {
    question: FAQ_Q4.en,
    answer: FAQ_A4.en,
  },
  {
    question: FAQ_Q5.en,
    answer: FAQ_A5.en,
  },
];

export const FAQTwitter = () => (
  <section className="main-bottom">
    <section className="faq" id="faq-block">
      <div className="container">
        <h2 className="heading-h2 faq__heading">FAQ</h2>
        {FAQs.map((faq, index) => (
          <FAQ
            question={faq.question}
            answer={faq.answer}
            open={faq.open}
            key={index}
          />
        ))}
      </div>
    </section>

    <div className="border" />

    <section className="twitter">
      <div className="container twitter__container">
        <h2 className="heading-h2 twitter__header">TWITTER</h2>
        <div className="twitter__tweets">
          <a
            className="twitter-timeline"
            href={links.twitter}
            data-height="800px"
          >
            Tweets by #SaveUkrainianFolk
          </a>
        </div>
        <a
          className="twitter__button twitter__link text-montserrat"
          href={links.twitter}
          target="_blank"
        >
          <FaTwitter />
          <span className="twitter__link-text">
            {MORE_IN_OUR_TWITTER_BUTTON_TEXT.en}
          </span>
        </a>
      </div>
    </section>
  </section>
);
