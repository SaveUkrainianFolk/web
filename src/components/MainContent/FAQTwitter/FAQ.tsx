import React, { useState } from 'react';

export interface FAQProps {
  question: string;
  answer: string;
  open?: boolean;
}

export const FAQ = ({ question, answer, open = false }: FAQProps) => {
  const [isOpen, setIsOpen] = useState(open);

  return (
    <div
      className={
        isOpen
          ? 'container-w faq__question-container faq__question-container--open'
          : 'container-w faq__question-container'
      }
    >
      <header className="faq__question" onClick={() => setIsOpen(!isOpen)}>
        <h5 className="text-aleksa">{question}</h5>
        <div className="faq__question-toggle text-aleksa">
          <span className="toggle-plus">+</span>
          <span className="toggle-minus">-</span>
        </div>
      </header>
      <footer>
        <p className="text-montserrat">{answer}</p>
      </footer>
    </div>
  );
};
