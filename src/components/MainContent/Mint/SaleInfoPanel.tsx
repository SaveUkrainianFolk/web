import React from 'react';
import { links, addresses } from '../../../constants';
import { useSaleInfo } from '../../../hooks';
import { AccountButton } from './Buttons';
import { NFT, Opensea } from '../../../assets/images';

import { useAccount } from 'wagmi';
import truncateEthAddress from 'truncate-eth-address';

export const SaleInfoPanel = () => {
  const { data: account } = useAccount();
  const {
    totalSupply = 0,
    maxFolks = 150,
    price = 0.1,
    totalDonated = 0,
    totalDonatedUSD = 0,
  } = useSaleInfo();

  const renderAccountButton = () => {
    if (!account) {
      return (
        <button className="text-aleksa button-red-m mint-block__button">
          Mint FOLKs
        </button>
      );
    }
    return <AccountButton />;
  };

  return (
    <section className="mint-block__statistic">
      <div className="mint-block__container mint-block__track">
        <img className="mint-block__album-cover" src={NFT} alt="NFT Cover" />
        <div>
          <h3 className="heading-h3 mint-block__h3">
            Shadows of Forgotten Ancestors
          </h3>
          <p className="text-montserrat mint-block__p">
            mixtape by
            <a
              target="_blank"
              className="text-aleksa color-r"
              href={links.mangataaqiuvera}
            >
              {' Mangata Aquivera'}
            </a>
          </p>
        </div>
      </div>
      <div className="mint-block__container mint-block__table-container mint-block__donate-container">
        <table className="mint-block__donate-table">
          <tbody>
            <tr>
              <td>CONTRACT ADDRESS</td>
              <td>
                <span className="mint-block__table-value">
                  <a
                    target="_blank"
                    href={`${links.etherscan}${addresses.ShadowsOfForgottenAncestors}`}
                  >
                    {truncateEthAddress(addresses.ShadowsOfForgottenAncestors)}
                  </a>
                </span>
              </td>
            </tr>
            <tr>
              <td>TOTAL NFTS</td>
              <td>
                <span className="mint-block__table-value">
                  {totalSupply} / {maxFolks}
                </span>
              </td>
            </tr>
            <tr>
              <td>PRICE PER NFT</td>
              <td>
                <span className="mint-block__table-value">{price} ETH</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="mint-block__container mint-block__table-container mint-block__total-container">
        <table className="mint-block__result-table">
          <tbody>
            <tr>
              <th>TOTAL DONATED</th>
              <th>USD VALUE</th>
            </tr>
            <tr className="color-r text-aleksa">
              <td>{totalDonated} ETH</td>
              <td>${totalDonatedUSD}</td>
            </tr>
          </tbody>
        </table>
        <progress className="mint-block__donate-bar" value="3" max="150" />
      </div>
      <button className="mint-block__opensea">
        <a href={links.opensea} target="_blank">
          <img src={Opensea} alt="opensea" />
        </a>
      </button>

      {renderAccountButton()}
    </section>
  );
};
