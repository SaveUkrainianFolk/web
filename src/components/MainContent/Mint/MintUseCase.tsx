import { useState, useEffect, useCallback } from 'react';
import { toast } from 'react-toastify';

import { useMint, useSaleInfo } from '../../../hooks';
import { links, addresses } from '../../../constants';
import { useAccount, useSigner, useNetwork } from 'wagmi';
import { ConnectButton } from './Buttons';
import truncateEthAddress from 'truncate-eth-address';

import { FaTwitter, FaInstagram } from 'react-icons/fa';
import { BiLoaderCircle } from 'react-icons/bi';
import { NFT, Opensea } from '../../../assets/images';

export enum MintStatus {
  NeedsToConnectWallet,
  NeedsToSwitchNetwork,
  ReadyToMint,
  LoadingMint,
  MintSucc,
  MintFail,
}

export const MintUseCase = () => {
  const { data: account } = useAccount();
  const { data: signer } = useSigner();
  const { activeChain } = useNetwork();

  /**
   * quantity
   */
  const [quantity, setQuantity] = useState(1);
  const {
    price = 0.1,
    maxFolksPerUser: maxQuantity = 5,
    backers,
  } = useSaleInfo();
  const [totalPrice, setTotalPrice] = useState(quantity * price);

  useEffect(() => {
    if (!quantity || !price) return;
    const _totalPrice = parseFloat((quantity * price).toFixed(1));
    setTotalPrice(_totalPrice);
  }, [quantity, price]);

  const handleQuantityChange = useCallback(
    (quantity: number) => {
      if (quantity > 0 && quantity <= maxQuantity) {
        setQuantity(quantity);
      }
    },
    [setQuantity]
  );

  const [isBacker, setIsBacker] = useState(false);

  useEffect(() => {
    if (!backers || !account) return;
    setIsBacker(
      backers.some(
        (backer) =>
          backer.toLowerCase() === account?.address?.toLocaleLowerCase()
      )
    );
  }, [backers, account]);

  /**
   * mint
   */
  const { mintData, mintError, isMintLoading, mint } = useMint({
    quantity,
    price: totalPrice,
  });

  /**
   * status
   */
  const [status, setStatus] = useState(MintStatus.NeedsToConnectWallet);

  useEffect(() => {
    let _status: MintStatus;
    if (!signer || activeChain?.unsupported) {
      _status = MintStatus.NeedsToConnectWallet;
    } else if (isMintLoading) {
      _status = MintStatus.LoadingMint;
    } else if (!mintData && !isBacker) {
      _status = MintStatus.ReadyToMint;
    } else if (mintData?.receipt?.status === 1 || isBacker) {
      _status = MintStatus.MintSucc;
    } else {
      _status = MintStatus.MintFail;
    }
    setStatus(_status);
  }, [signer, activeChain, mintData, isMintLoading, isBacker]);

  useEffect(() => {
    if (!mintError) return;
    const txMessage = (mintError as Error).message;
    toast.error(txMessage);
  }, [mintError]);

  const twitterShareLink = `https://twitter.com/intent/tweet?url=https://opensea.io/assets/ethereum/${
    addresses.ShadowsOfForgottenAncestors
  }/${
    mintData ? mintData.nftId : 0
  }&text=I've%20just%20donated%20to%20Ukraine%20by%20minting%20FOLK%20NFT%20-%20the%20hour%20mixtape%20of%20Ukrainian%20folk%20music!`;

  /**
   * views
   */
  const renderConnectWalletBlock = () => (
    <div className="mint-block__connect-wallet">
      <ConnectButton />
    </div>
  );

  const renderQuantityBlock = () => (
    <div className="mint-block__quantity">
      <div className="mint-block__quantity-counter text-aleksa color-w">
        <span className="mint-block__quantity-counter-title">
          FOLKs Quantity
        </span>
        <div
          className="text-aleksa mint-block__counter-button mint-block__counter-button--minus"
          onClick={() => handleQuantityChange(quantity - 1)}
        >
          -
        </div>
        <span className="mint-block__counter-value">{quantity}</span>
        <div
          className="text-aleksa mint-block__counter-button mint-block__counter-button--plus"
          onClick={() => handleQuantityChange(quantity + 1)}
        >
          +
        </div>
        <span>{totalPrice} ETH</span>
      </div>
      <button
        className="text-aleksa button-black-m mint-block__mint-button"
        onClick={() => mint()}
      >
        Mint
      </button>
    </div>
  );

  const renderLoadingMintBlock = () => (
    <div className="mint-block__loading">
      <BiLoaderCircle className="mint-block__loader" />
      <span className="color-w text-aleksa mint-block__loading-span">
        Waiting for transaction...
      </span>
      {mintData?.tx && (
        <a href={`https://etherscan.io/tx/${mintData.tx.hash}`} target="_blank">
          <span className="mint-block__thanks-transaction">
            <span className="text-montserrat color-w mint-block__transaction-code">
              {truncateEthAddress(mintData.tx.hash)}
            </span>
          </span>
        </a>
      )}
    </div>
  );

  const renderMintFailBlock = () => (
    <div className="mint-block__tx-fail">
      <span className="color-w text-aleksa mint-block__tx-fail-span">
        Transaction
      </span>
      {mintData?.tx && (
        <a href={`https://etherscan.io/tx/${mintData.tx.hash}`} target="_blank">
          <span className="mint-block__thanks-transaction">
            <span className="text-montserrat color-w mint-block__transaction-code">
              {truncateEthAddress(mintData.tx.hash)}
            </span>
          </span>
        </a>
      )}
      <span className="color-w text-aleksa mint-block__tx-fail-span">
        {' '}
        failed 🥲
      </span>
      <button
        className="text-aleksa button-black-m mint-block__tx-fail-button"
        // onClick={() => resetTransaction()}
      >
        Try Again
      </button>
    </div>
  );

  const renderMintSuccBlock = () => (
    <div className="mint-block__complete">
      <div className="mint-block__transaction">
        <div className="mint-block__thanks-container">
          <p className="color-w text-aleksa mint-block__thanks">
            Thank you! The minting was successful.
          </p>
          {mintData?.tx && (
            <a
              href={`https://etherscan.io/tx/${mintData.tx.hash}`}
              target="_blank"
            >
              <p className="text-montserrat color-w mint-block__thanks-transaction">
                Transaction:
                <span className="mint-block__transaction-code">
                  {truncateEthAddress(mintData.tx.hash)}
                </span>
              </p>
            </a>
          )}
        </div>
        <a
          href={
            mintData?.nftId
              ? `https://opensea.io/assets/ethereum/${addresses.ShadowsOfForgottenAncestors}/${mintData.nftId}`
              : `https://opensea.io/${account?.address}`
          }
          target="_blank"
        >
          <button className="text-aleksa button-black-m mint-block__see-on">
            Open on <img src={Opensea} alt="opensea" />
          </button>
        </a>
      </div>
      {/* <div className="mint-block__comment">
        <div className="mint-block__comment-nft">
          <img
            className="mint-block__comment-album"
            src={NFT}
            alt="NFT Cover"
          />
          <span className="color-w text-montserrat mint-block__nft-title">
            #SaveUkrainianFolk
            <p className="color-w text-aleksa">
              Shadows of Forgotten Ancestors
            </p>
          </span>
        </div>
        <div className="mint-block__comment-leave">
          <button className="text-aleksa button-black-m mint-block__comment-button">
            Leave Comment
          </button>
          <p className="text-montserrat color-w">
            The possibility of commenting is open to NFT holders.
          </p>
        </div>
      </div> */}
      <div className="mint-block__social">
        <p className="color-w text-aleksa">
          Share your most meaningful NFT at socials! Help us #SaveUkrainianFolk
        </p>
        <div className="mint-block__social-links-container">
          <button className="mint-block__social-link">
            <a href={twitterShareLink} target="_blank">
              <FaTwitter />
            </a>
          </button>
          <button className="mint-block__social-link">
            <a href={links.instagram} target="_blank">
              <FaInstagram />
            </a>
          </button>
        </div>
      </div>
    </div>
  );

  const renderMintUseCase = () => {
    if (
      status === MintStatus.NeedsToConnectWallet ||
      status === MintStatus.NeedsToSwitchNetwork
    ) {
      return renderConnectWalletBlock();
    }
    if (status === MintStatus.ReadyToMint) {
      return renderQuantityBlock();
    }
    if (status === MintStatus.LoadingMint) {
      return renderLoadingMintBlock();
    }
    if (status === MintStatus.MintFail) {
      return renderMintFailBlock();
    }
    if (status === MintStatus.MintSucc) {
      return renderMintSuccBlock();
    }
  };

  return (
    <footer className="mint-block__footer">
      <div className="mint-block__footer-container">{renderMintUseCase()}</div>
    </footer>
  );
};
