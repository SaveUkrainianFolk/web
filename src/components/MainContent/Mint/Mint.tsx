import React from 'react';
import { SaleInfoPanel, MintUseCase } from '.';
import { MINT_TITLE_TEXT } from '../../../constants';

export const Mint = () => (
  <section className="mint-block mint-block--open-footer" id="mint-block">
    <div className="container">
      <h2 className="heading-h2 mint-block__heading">
        {`${MINT_TITLE_TEXT.en} `}
        <span className="color-r mint-block__hashtag">#saveukrainianfolk</span>
      </h2>
      <SaleInfoPanel />
      <MintUseCase />
    </div>
  </section>
);
