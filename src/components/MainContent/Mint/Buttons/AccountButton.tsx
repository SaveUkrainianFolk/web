import { ConnectButton as RainbowButton } from '@rainbow-me/rainbowkit';

export const AccountButton = () => {
    return (
        <div className='mint-block__account-button'>
            <RainbowButton.Custom>
                {({
                      account,
                      chain,
                      openAccountModal,
                      openChainModal,
                      openConnectModal,
                      mounted,
                  }) => {
                    if (mounted && account && chain) {
                        return (
                            <div style={{display: 'flex', gap: 12}}>
                                <button onClick={openAccountModal} type="button"
                                        className='text-aleksa button-red-m mint-block__button'>
                                    {account.displayName}
                                    {/*{account.displayBalance*/}
                                    {/*? ` (${account.displayBalance})`*/}
                                    {/*: ''}*/}
                                </button>
                            </div>
                        );
                    }
                }}
            </RainbowButton.Custom>
        </div>
    );
};
