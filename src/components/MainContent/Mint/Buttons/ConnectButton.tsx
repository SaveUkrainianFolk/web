import { ConnectButton as RainbowConnectButton } from '@rainbow-me/rainbowkit';

export const ConnectButton = () => {
    return (
        <RainbowConnectButton.Custom>
            {({
                  account,
                  chain,
                  openAccountModal,
                  openChainModal,
                  openConnectModal,
                  mounted,
              }) => {
                if (!mounted || !account || !chain) {
                    return (
                        <button onClick={openConnectModal} type="button"
                                className='text-aleksa button-black-m mint-block__connect-wallet-button'>
                            Connect Wallet
                        </button>
                    );
                }
                if (chain.unsupported) {
                    return (
                        <button onClick={openChainModal} type="button"
                                className='text-aleksa button-black-m mint-block__connect-wallet-button'>
                            Switch network
                        </button>
                    );
                }
            }}
        </RainbowConnectButton.Custom>
    );
};
