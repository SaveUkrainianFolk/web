import React from 'react';
import { Member } from './';
import { FaTwitter } from 'react-icons/fa';
import { TEAM_DESCRIPTION_TEXT, TEAM_TITLE_TEXT } from '../../../constants';
import { TEAM_MEMBERS } from '../../../constants/team';
import { isMobile } from '../../../utils';
// import { VideoPlayer } from '../../Common';
// import MissionVideo from '../../../assets/videos/mission.mp4'; // replace with team video
// import { PlayButton } from '../../../assets/images';

export const Team = () => {
  const Max = TEAM_MEMBERS[0];

  return (
    <section className="team" id="team-block">
      <div className="team__container">
        <h2 className="heading-h2">{TEAM_TITLE_TEXT.en}</h2>

        <div className="team__gallery">
          <div className="team__gallery-container">
            {TEAM_MEMBERS.filter(
              (_, index) => (index !== 0 && !isMobile) || (isMobile && true)
            ).map((member, index) => (
              <Member
                key={index}
                name={member.name}
                surname={member.surname}
                imgSrc={member.imgSrc}
                position={member.position}
                twitter={member.twitter}
                instagram={member.instagram}
                active={member.active}
              />
            ))}
          </div>
        </div>

        {!isMobile && (
          <div className="popup__member-card-container">
            <div className="popup__member-card">
              <div className="popup__member-photo-container">
                <div
                  className="popup__team-member"
                  style={{ backgroundImage: `url(${Max.imgSrc})` }}
                />
                <a
                  className="popup__member-social-button text-montserrat"
                  href={Max.twitter}
                  target="_blank"
                >
                  <FaTwitter />
                  Twitter
                </a>
              </div>
              <div className="popup__member-profile">
                <div className="popup__member-title">
                  <span className="text-aleksa popup__member-card-name">
                    {`${Max.name} ${Max.surname}`}
                  </span>
                  <span className="text-montserrat popup__member-card-role">
                    {Max.position}
                  </span>
                </div>
                <p className="popup__member-info text-montserrat">
                  {TEAM_DESCRIPTION_TEXT.en}
                </p>
                {/* <div className="popup__member-video">
                <VideoPlayer
                  videoUrl={MissionVideo}
                  previewImgSrc={Max.imgSrc}
                />
              </div> */}
              </div>
            </div>
          </div>
        )}

        {isMobile && (
          <div className="container">
            <p className="text-montserrat team__team">
              {TEAM_DESCRIPTION_TEXT.en}
            </p>
            {/* <button className="text-aleksa button-black-l team__team-button">
            Our Team
          </button> */}
          </div>
        )}
      </div>
    </section>
  );
};
