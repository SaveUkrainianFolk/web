import React, { useState } from 'react';
import { FaInstagram, FaTwitter } from 'react-icons/fa';

export interface MemberProps {
  name: string;
  surname: string;
  imgSrc: string;
  position: string;
  instagram?: string;
  twitter?: string;
  active?: boolean;
}

export const Member = ({
  name,
  surname,
  position,
  imgSrc,
  instagram,
  twitter,
  active = false,
}: MemberProps) => {
  const [isActive, setIsActive] = useState(active);

  return (
    <div
      className={
        isActive
          ? 'team__gallery-item team__gallery-item--active'
          : 'team__gallery-item'
      }
      style={{ backgroundImage: `url(${imgSrc})` }}
      onMouseEnter={() => setIsActive(true)}
      onMouseLeave={() => setIsActive(active)}
    >
      <span className="team__gallery-member-name text-aleksa">
        {name} <span className="team__gallery-member-surname">{surname}</span>
      </span>
      <span className="team__gallery-member-position text-montserrat">
        {position}
      </span>
      {(!!twitter || !!instagram) && (
        <a href={twitter || instagram} target="_blank">
          <button className="popup__member-social">
            {twitter ? <FaTwitter /> : <FaInstagram />}
          </button>
        </a>
      )}
    </div>
  );
};
