import React, { useState } from 'react';
import {
  AUDIENCE_LOAD_MORE_BUTTON_TEXT,
  AUDIENCE_SUBTITLE_TEXT,
  AUDIENCE_TITLE_TEXT,
} from '../../../constants';
import { useSaleInfo } from '../../../hooks';
import { Backer } from './';

export const Audience = () => {
  const { backers } = useSaleInfo();
  const [isLoadMore, setIsLoadMore] = useState(false);
  const previewBackersNumber = 10;

  return (
    <section className="audience" id="audience-block">
      <div className="container">
        <h2 className="heading-h2 audience__heading">
          {AUDIENCE_TITLE_TEXT.en}
        </h2>
        <p className="text-montserrat audience__p">
          {AUDIENCE_SUBTITLE_TEXT.en}
        </p>
        <div className="container-w">
          <ul className="audience__list">
            {backers &&
              backers
                .slice(0, !isLoadMore ? previewBackersNumber : backers.length)
                .map((backerAddress, index) => (
                  <Backer
                    address={backerAddress}
                    number={index + 1}
                    key={index}
                  />
                ))}
          </ul>
        </div>
        {!isLoadMore && backers && backers.length > previewBackersNumber ? (
          <div className="audience__button-container">
            <button
              className="text-aleksa button-white-l"
              onClick={() => setIsLoadMore(true)}
            >
              {AUDIENCE_LOAD_MORE_BUTTON_TEXT.en}
            </button>
          </div>
        ) : null}
      </div>
    </section>
  );
};
