import React from 'react';
import { links } from '../../../constants';
import { Logo } from '../../../assets/images';
import { useEnsAvatar, useEnsName } from 'wagmi';
import truncateEthAddress from 'truncate-eth-address';

export interface BackerProps {
  username?: string;
  address: string;
  number: number;
}

export const Backer = ({ username, address, number }: BackerProps) => {
  const { data: ensAvatar } = useEnsAvatar({
    addressOrName: address,
  });
  const { data: ensName } = useEnsName({
    address: address,
  });

  return (
    <li className="audience__item">
      <img
        className="audience__avatar"
        src={ensAvatar ? ensAvatar : Logo}
        alt="Backer avatar"
      />
      <a target="_blank" href={`${links.etherscan}${address}`}>
        <span className="text-aleksa audience__name">
          {ensName || truncateEthAddress(address)}
        </span>
        {ensName ? (
          <span className="text-montserrat audience__span audience__span-address">
            {truncateEthAddress(address)}
          </span>
        ) : null}
      </a>
      <span className="text-montserrat audience__span">#{number}</span>
    </li>
  );
};
