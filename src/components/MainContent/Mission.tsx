import React from 'react';
// @ts-ignore
import { Link } from 'react-scroll';
import { VideoPlayer } from '../Common';
import { MissionVideoPreview } from '../../assets/images';
import MissionVideo from '../../assets/videos/mission.mp4';
import {
  GOTO_MINT_BUTTON_TEXT,
  MISSION_P1_TEXT,
  MISSION_P2_TEXT,
  MISSION_P3_TEXT,
  MISSION_SUBTITLE_TEXT,
  MISSION_TITLE_TEXT,
  MISSION_VIDEO_SUBHEADER_TEXT,
} from '../../constants';

export const Mission = () => (
  <section className="mission" id="mission-block">
    <div className="container mission__container">
      <div className="mission__container-col">
        <div className="mission__block">
          <h2 className="heading-h2">{MISSION_TITLE_TEXT.en}</h2>
          <p className="text-montserrat mission__p">{MISSION_P1_TEXT.en}</p>
          <p className="text-aleksa mission__p mission__link">
            #SAVEUKRAINIANFOLK
          </p>
          <p className="text-montserrat mission__p">{MISSION_P2_TEXT.en}</p>
        </div>
        <div className="mission__block">
          <h3 className="heading-h3 mission__h3">{MISSION_SUBTITLE_TEXT.en}</h3>
          <p className="text-montserrat mission__p">{MISSION_P3_TEXT.en}</p>
          <Link
            className="button-red-l text-aleksa mission__block-button"
            to="mint-block"
            spy={true}
            smooth={true}
          >
            {GOTO_MINT_BUTTON_TEXT.en}
          </Link>
        </div>
      </div>
      <div className="mission__video-block">
        <div className="mission__video-container">
          <VideoPlayer
            videoUrl={MissionVideo}
            previewImgSrc={MissionVideoPreview}
          />
        </div>
        <p className="text-montserrat button-black-l">
          {MISSION_VIDEO_SUBHEADER_TEXT.en}
        </p>
      </div>
    </div>
  </section>
);
