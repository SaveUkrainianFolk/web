import React from 'react';
import { BANNER_P1_TEXT, BANNER_P2_TEXT } from '../../constants';

export const Banner = () => (
  <section className="banner">
    <div className="banner__container">
      <button className="text-aleksa button-black-m banner__container-button">
        #SaveUkrainianFolk
      </button>
      <p className="text-montserrat color-w banner__p">{BANNER_P1_TEXT.en}</p>
      <p className="text-montserrat color-w banner__p">{BANNER_P2_TEXT.en}</p>
    </div>
  </section>
);
