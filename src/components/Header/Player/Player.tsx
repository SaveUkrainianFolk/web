import React, { useState, useRef, ReactNode, useEffect } from 'react';
import { FaVolumeUp } from 'react-icons/fa';
import { NFT } from '../../../assets/images';
import { FaPlay } from 'react-icons/fa';

export interface PlayerProps {
  isPlaying: boolean;
  toggle: () => void;
  progress: number;
  handleProgressChange: (progress: number) => void;
}

export const Player = ({
  isPlaying,
  toggle,
  progress,
  handleProgressChange,
}: PlayerProps) => {
  const marker = useRef<HTMLDivElement>(null);
  const progressBar = useRef<HTMLProgressElement>(null);
  const [isHoldingMarker, setIsHoldingMarker] = useState(false);
  const showProgressBar = false;

  useEffect(() => {
    document.onmousemove = dragPlayerMarket;
    document.onmouseup = handleMouseUp;
  });

  const dragPlayerMarket = (event: MouseEvent) => {
    if (
      !isHoldingMarker ||
      !marker.current ||
      event.clientX < 0 ||
      event.clientX > window.innerWidth ||
      !progressBar.current
    )
      return;
    const draggedProgress = (event.clientX / window.innerWidth) * 100;
    marker.current.style.left = progressToPxValue(draggedProgress);
    progressBar.current.value = draggedProgress;
  };

  const handleMarketMouseDown = () => {
    if (isPlaying) toggle();
    setIsHoldingMarker(true);
  };

  const handleMouseUp = (event: MouseEvent) => {
    if (isHoldingMarker) {
      const draggedProgress = (event.clientX / window.innerWidth) * 100;
      handleProgressChange(draggedProgress);
    }
    setIsHoldingMarker(false);
  };

  const progressToPxValue = (progress: number) => {
    return `${(window.innerWidth * progress) / 100}px`;
  };

  return (
    <footer className="footer-player">
      {showProgressBar && (
        <div className="footer-player__progress">
          <progress
            className="footer-player__progress-bar"
            ref={progressBar}
            value={progress}
            max="100"
          />
          <div
            className="footer-player__marker"
            ref={marker}
            style={{ left: progressToPxValue(progress) }}
            onMouseDown={handleMarketMouseDown}
          />
        </div>
      )}
      <div className="container footer-player__container">
        <img className="footer-player__album-cover" src={NFT} alt="" />
        <div className="footer-player__control-button" onClick={toggle}>
          {!isPlaying ? (
            <FaPlay />
          ) : (
            <div style={{ display: 'contents' }}>
              <span className="footer-player__control-item" />
              <span className="footer-player__control-item" />
            </div>
          )}
        </div>
        <div className="footer-player__track">
          <div className="footer-player__track-container">
            <p className="footer-player__artist text-aleksa">
              Shadows of Forgotten Ancestors
            </p>
            <p className="text-montserrat footer-player__song">
              #SaveUkrainianFolk
            </p>
          </div>
          <span className="text-montserrat color-w footer-player__track-time">
            49:38
          </span>
        </div>
      </div>
    </footer>
  );
};
