import React from 'react';
import Logo from '../../assets/images/logo.png';

export const Comment = () => (
  <div className="popup-msg">
    <div className="popup-msg__header text-montserrat">
      <img src={Logo} alt="" />
      <span>@billi_herrington</span>
      <span className="popup-msg__code">0x6578</span>
      <span className="popup-msg__date">6 days ago</span>
    </div>
    <p className="popup-msg__text text-montserrat">
      Over the last 30 years, as I made my way through my career, I came to a
      realization. I always made my music for my fans. But they never got a
      chance to be a part of it. They only watched the Dogg from afar.
    </p>
    <div className="popup-msg__footer text-montserrat">
      <div>
        <div className="popup-msg__footer-row">
          <span>Ranked</span>
          <span className="popup-msg__bordered-span">#126 of 1000</span>
        </div>
        <div>
          <span>Contributed</span>
          <span className="popup-msg__bordered-span">0.1 ETH</span>
        </div>
      </div>
      <div>
        <span>Left at</span>
        <span className="popup-msg__bordered-span">08:58</span>
      </div>
    </div>
  </div>
);
