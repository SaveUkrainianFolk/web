import React, { useState, useRef } from 'react';
import { Navbar } from './';
// @ts-ignore
import { Link } from 'react-scroll';
import { addNftImageAnimation, isMobile } from '../../utils';
import {
  NFT,
  MangataaqiuveraW,
  HeaderBackground,
  Opensea,
} from '../../assets/images';
import IntroVideo from '../../assets/videos/intro.mp4';
import ReactPlayer from 'react-player';
import {
  HEADER_DESCRIPTION_TEXT,
  HEADER_MIXTAPE_BY_TEXT,
  links,
} from '../../constants';
import { FaPlay } from 'react-icons/fa';
import Spectrum from '../../assets/images/spectrum.svg';
import SpectrumRed from '../../assets/images/spectrum-red.svg';
import SpectrumReverse from '../../assets/images/spectrum-reverse.svg';
import SOFAMp3 from '../../assets/audios/shadows-of-forgotten-ancestors.mp3';
import { Player } from '..';

export const Header = ({
  handlePlayerOpen,
}: {
  handlePlayerOpen: () => void;
}) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [playingProgress, setPlayingProgress] = useState(0);
  const audioPlayer = useRef<ReactPlayer>(null);
  const videoPlayer = useRef<ReactPlayer>(null);

  const togglePlaying = () => {
    if (!audioPlayer.current) return;
    setIsPlaying(!isPlaying);
  };

  const handleAudioListen = () => {
    if (!audioPlayer.current) return;
    const progress =
      (audioPlayer.current.getCurrentTime() /
        audioPlayer.current.getDuration()) *
      100;
    setPlayingProgress(progress);
  };

  const handleProgressClick = (event: React.MouseEvent) => {
    const newPlayingProgress = (event.clientX / window.innerWidth) * 100;
    changePlayingProgress(newPlayingProgress);
  };

  const changePlayingProgress = (progress: number) => {
    if (!audioPlayer.current) return;
    const newCurrentTime = (audioPlayer.current.getDuration() * progress) / 100;
    audioPlayer.current.seekTo(newCurrentTime);
    setPlayingProgress(progress);
    if (!isPlaying) togglePlaying();
    if (!videoPlayer.current) return;
    if (videoPlayer.current.getDuration() > newCurrentTime) {
      videoPlayer.current.seekTo(newCurrentTime);
    }
  };

  const isVideoPlayerVisible = () => {
    if (!audioPlayer.current || !videoPlayer.current) return;
    return (
      isPlaying &&
      (audioPlayer.current.getCurrentTime() <
        videoPlayer.current.getDuration() ||
        !audioPlayer.current.getCurrentTime())
    );
  };

  const renderVideoPlayer = () => (
    <ReactPlayer
      url={IntroVideo}
      ref={videoPlayer}
      muted={true}
      config={{
        file: {
          attributes: { poster: HeaderBackground, preload: 'none' },
        },
      }}
      playing={isPlaying}
      style={{ visibility: isVideoPlayerVisible() ? 'visible' : 'hidden' }}
    />
  );

  const renderAudioPlayer = () => (
    <ReactPlayer
      url={SOFAMp3}
      ref={audioPlayer}
      onProgress={handleAudioListen}
      progressInterval={200}
      volume={0.5}
      width={0}
      height={0}
      playing={isPlaying}
      onPlay={handlePlayerOpen}
      config={{
        file: {
          attributes: { preload: 'none' },
        },
      }}
    />
  );

  return (
    <header className="header">
      {renderAudioPlayer()}

      <div className="header__preview">
        <div className="container header__container">
          <Navbar />
          <div className="header__player">
            <button
              className="header-player__control"
              onClick={togglePlaying}
              style={{ opacity: isMobile && isPlaying ? '0.33' : '1' }}
            >
              {!isPlaying ? (
                <FaPlay />
              ) : (
                <div style={{ display: 'contents' }}>
                  <span className="header-player__control-item" />
                  <span className="header-player__control-item" />
                </div>
              )}
            </button>
            <img
              className="header-player__cover"
              src={NFT}
              alt="Shadows of Forgotten Ancestors Cover"
              id="nft"
              onLoad={addNftImageAnimation}
            />
            <div className="header-player__title-container">
              <h1 className="text-aleksa header-player__title">
                Shadows of Forgotten Ancestors
              </h1>
              <div className="header-player__mixtape-container">
                <span className="text-montserrat color-w header-player__mixtape-by">
                  {HEADER_MIXTAPE_BY_TEXT.en}
                </span>
                <a
                  href={links.mangataaqiuvera}
                  target="_blank"
                  className="text-aleksa header-player__album-button"
                >
                  <img src={MangataaqiuveraW} alt="" />
                  Mangata Aquivera
                </a>
              </div>
              <div className="header-player__opensea-container">
                <a
                  href={links.opensea}
                  target="_blank"
                  className="text-aleksa header-player__album-button"
                >
                  <img src={Opensea} alt="" />
                  OpenSea
                </a>
              </div>
            </div>
            <p className="color-w text-montserrat header-player__description">
              {HEADER_DESCRIPTION_TEXT['en']}
            </p>
            <Link
              className="button-red-l text-aleksa header-player__mint-button"
              to="mint-block"
              spy={true}
              smooth={true}
            >
              Mint to #SaveUkrainianFolk
            </Link>
          </div>
        </div>

        {!isMobile && (
          <div className="header__video" onClick={togglePlaying}>
            {renderVideoPlayer()}
          </div>
        )}

        <div
          className="header__spectrum header__spectrum-inactive"
          onClick={(e) => handleProgressClick(e)}
        >
          <img src={Spectrum} />
        </div>
        <div
          className="header__spectrum header__spectrum-active"
          onClick={(e) => handleProgressClick(e)}
          style={{
            clipPath: `polygon(0 0, ${playingProgress}% 0, ${playingProgress}% 100%, 0% 100%)`,
          }}
        >
          <img src={SpectrumRed} />
        </div>
        <div className="header__spectrum-duration text-montserrat color-w">
          60:00
        </div>
      </div>

      <div className="header__spectrum header__spectrum-reverse">
        <img src={SpectrumReverse} />
      </div>

      {/* <section className="twitter-comment">
        <div className="container-w twitter-comment__container">
          <img src={Logo} alt="" className="twitter-user" />
          <p className="text-montserrat twitter-comment__p">
            <span className="text-aleksa">Write a comment at</span>
            <span className="color-r text-aleksa">{' 23:46'}</span>
          </p>
        </div>
      </section> */}

      {playingProgress > 0 && (
        <Player
          isPlaying={isPlaying}
          toggle={togglePlaying}
          progress={playingProgress}
          handleProgressChange={changePlayingProgress}
        />
      )}
    </header>
  );
};
