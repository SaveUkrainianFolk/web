import React from 'react';
// @ts-ignore
import { Link } from 'react-scroll';
import { FaTwitter, FaInstagram, FaGitlab } from 'react-icons/fa';
import { IoIosCloseCircle } from 'react-icons/io';
import { Logo, Opensea } from '../../assets/images';
import {
  HEADER_NAVBAR_AUDIENCE_TEXT,
  HEADER_NAVBAR_FAQ_TEXT,
  HEADER_NAVBAR_MINT_TEXT,
  HEADER_NAVBAR_MISSION_TEXT,
  HEADER_NAVBAR_TEAM_TEXT,
  links,
} from '../../constants';

export const Navbar = () => (
  <div className="header__navbar">
    <div className="header__logo">
      <img className="header__logo-img" src={Logo} alt="Logo" />
      <span className="color-r text-aleksa header__logo-title">
        Save Ukrainian Folk
      </span>
    </div>
    <div className="header__nav-container">
      <h3 className="text-aleksa color-w header__nav-h3">NAVIGATION</h3>
      <nav className="header__nav">
        <ul className="header__nav-list">
          <li className="header__nav-item">
            <Link
              className="color-w text-montserrat"
              to="mission-block"
              spy={true}
              smooth={true}
            >
              {HEADER_NAVBAR_MISSION_TEXT.en}
            </Link>
          </li>
          <li className="header__nav-item">
            <Link
              className="color-w text-montserrat"
              to="mint-block"
              spy={true}
              smooth={true}
            >
              {HEADER_NAVBAR_MINT_TEXT.en}
            </Link>
          </li>
          <li className="header__nav-item">
            <Link
              className="color-w text-montserrat"
              to="audience-block"
              spy={true}
              smooth={true}
            >
              {HEADER_NAVBAR_AUDIENCE_TEXT.en}
            </Link>
          </li>
          <li className="header__nav-item">
            <Link
              className="color-w text-montserrat"
              to="team-block"
              spy={true}
              smooth={true}
            >
              {HEADER_NAVBAR_TEAM_TEXT.en}
            </Link>
          </li>
          <li className="header__nav-item">
            <Link
              className="color-w text-montserrat"
              to="faq-block"
              spy={true}
              smooth={true}
            >
              {HEADER_NAVBAR_FAQ_TEXT.en}
            </Link>
          </li>
        </ul>
      </nav>
    </div>
    {/* <div className="header__lang-container">
      <button className="text-aleksa header__lang header__lang--active">
        Eng<span className="header__lang-show">lish</span>
      </button>
      <button className="text-aleksa header__lang">
        Укр<span className="header__lang-show">аїнською</span>
      </button>
    </div> */}
    {/* <button className="header__menu-button" id="menu-button">
      <div className="header__menu-burger">
        <div className="header__menu-burger-item" />
        <div className="header__menu-burger-item" />
      </div>
      <div className="header__menu-cross">
        <IoIosCloseCircle />
      </div>
    </button> */}
    <div className="header__social">
      <a href={links.opensea} target="_blank" className="header__social-link">
        <img src={Opensea} alt="OpenSea" />
        <span className="text-aleksa color-w header__social-name">Twitter</span>
      </a>
      <a href={links.twitter} target="_blank" className="header__social-link">
        <FaTwitter />
        <span className="text-aleksa color-w header__social-name">Twitter</span>
      </a>
      <a href={links.instagram} target="_blank" className="header__social-link">
        <FaInstagram />
        <span className="text-aleksa color-w header__social-name">
          Instagram
        </span>
      </a>
      <a href={links.gitlab} target="_blank" className="header__social-link">
        <FaGitlab />
        <span className="text-aleksa color-w header__social-name">GitLab</span>
      </a>
    </div>
  </div>
);
