import React, { useState } from 'react';
import ReactPlayer from 'react-player';
import { PlayButton } from '../../assets/images';

export const VideoPlayer = ({
  videoUrl,
  previewImgSrc,
}: {
  videoUrl: string;
  previewImgSrc: string;
}) => {
  const [isPlaying, setIsPlaying] = useState(false);

  const tooglePlaying = () => {
    setIsPlaying(!isPlaying);
  };

  return (
    <div style={{ display: 'contents' }} onClick={tooglePlaying}>
      <ReactPlayer
        url={videoUrl}
        playing={isPlaying}
        width="100%"
        height="100%"
        light={previewImgSrc}
        playIcon={
          <button className="play-button">
            <img src={PlayButton} alt="Play Button" />
          </button>
        }
        config={{ file: { attributes: { preload: 'none' } } }}
      />
    </div>
  );
};
