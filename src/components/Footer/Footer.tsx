import React from 'react';
import { FaTwitter, FaInstagram, FaGitlab } from 'react-icons/fa';
import { LoveFromUa, Opensea, MangataaqiuveraB } from '../../assets/images';
import { FOOTER_TITLE_TEXT, links } from '../../constants';

export const Footer = () => (
  <footer className="footer">
    <div className="footer__container">
      <div className="footer__description-container">
        <img className="footer__heart" src={LoveFromUa} alt="Love from UA" />
        <p className="text-aleksa footer__title">{FOOTER_TITLE_TEXT.en}</p>
        <p className="text-montserrat footer__copyrights">
          &#169; 2022 #SaveUkrainianFolk.org
        </p>
      </div>
      <div className="footer__social">
        <a href={links.opensea} target="_blank" className="footer__social-link">
          <img src={Opensea} className="opensea" alt="Opensea" />
        </a>
        <a href={links.twitter} target="_blank" className="footer__social-link">
          <FaTwitter />
        </a>
        <a
          href={links.instagram}
          target="_blank"
          className="footer__social-link"
        >
          <FaInstagram />
        </a>
        <a href={links.gitlab} target="_blank" className="footer__social-link">
          <FaGitlab />
        </a>
        <a
          href={links.mangataaqiuvera}
          target="_blank"
          className="footer__social-link"
        >
          <img src={MangataaqiuveraB} alt="Mangata Aquivera" />
        </a>
      </div>
    </div>
  </footer>
);
