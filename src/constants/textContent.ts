export const HEADER_NAVBAR_MISSION_TEXT = {
  en: `Mission`,
  ua: `Місія`,
};
export const HEADER_NAVBAR_MINT_TEXT = {
  en: `Mint`,
  ua: `Мінт`,
};
export const HEADER_NAVBAR_AUDIENCE_TEXT = {
  en: `Audience`,
  ua: `Аудиторія`,
};
export const HEADER_NAVBAR_TEAM_TEXT = {
  en: `Team`,
  ua: `Команда`,
};
export const HEADER_NAVBAR_FAQ_TEXT = {
  en: `FAQ`,
  ua: `FAQ`,
};
export const HEADER_MIXTAPE_BY_TEXT = {
  en: `mixtape by`,
  ua: `мікстейп від`,
};
export const HEADER_DESCRIPTION_TEXT = {
  en: `1-hour mixtape of handmade Ukrainian folk, as a charitable NFT!
   Mint it to help us save our ethnic culture and raise funds for humanitarian support of the Ukrainian people.
    Funds are donated to Unchain Fund.`,
  ua: ``,
};
export const GOTO_MINT_BUTTON_TEXT = {
  en: `Mint to #SaveUkrainianFolk`,
  ua: `Мінт #SaveUkrainianFolk`,
};
export const MISSION_TITLE_TEXT = {
  en: `OUR MISSION`,
  ua: `НАША МІСІЯ`,
};
export const MISSION_P1_TEXT = {
  en: `To save Ukrainians culture and lives by bringing art, inclusion, and meaningfulness into web3.`,
  ua: ``,
};
export const MISSION_P2_TEXT = {
  en: `Since 24th Feb of 2022, when russia started the war against Ukraine,
   all 44 million of our people have been living as a single organism.
   Every second of life each of us thinks about how to help and do something.
    Army, work, art, volunteering, self-care, online activities, web3 projects...`,
  ua: ``,
};
export const MISSION_SUBTITLE_TEXT = {
  en: `The most meaningful NFT you mint`,
  ua: ``,
};
export const MISSION_P3_TEXT = {
  en: `We created a project at the intersection of national art, charity, and web3.
   And 3 things it does: spread our ethnic culture across the world, bring inclusion and sense
   into the web3 community, and raise funds for the humanitarian needs of the Ukrainian people.`,
  ua: ``,
};
export const MISSION_VIDEO_SUBHEADER_TEXT = {
  en: `What's happening in Ukraine now.`,
  ua: ``,
};
export const BANNER_P1_TEXT = {
  en: `Why save folk? For hundreds of years, Ukrainian national culture has been under repression.
   They've been trying to ban our language, our folklore, our ethnic identity, our rituals.
    We do not want to lose our roots. We do not want to forget our ancestors. We all gonna save it! `,
  ua: ``,
};
export const BANNER_P2_TEXT = {
  en: `80% of raised funds are automatically transferred from our smart contract to Unchain Fund 
  and the rest 20% goes to the team, artists, and our families.
   The Ethereum blockchain and open-sourced code provide transparency to #SaveUkrainianFolk`,
  ua: ``,
};
export const MINT_TITLE_TEXT = {
  en: `MINT TO`,
  ua: `МІНТ`,
};
export const AUDIENCE_TITLE_TEXT = {
  en: `AUDIENCE`,
  ua: `АУДИТОРІЯ`,
};
export const AUDIENCE_SUBTITLE_TEXT = {
  en: `By purchasing a limited edition NFT of the FOLK mixtape, 
  supporters claim a permanent seat in the audience and special access
   to future projects, activities, and art airdrops from Ukraine.`,
  ua: ``,
};
export const AUDIENCE_LOAD_MORE_BUTTON_TEXT = {
  en: `Load More`,
  ua: `Показати інших`,
};
export const TEAM_TITLE_TEXT = {
  en: `Who we are`,
  ua: `Хто ми`,
};
export const TEAM_DESCRIPTION_TEXT = {
  en: `We are friends from different parts of Ukraine. Each of us used to live our best life,
   make art and tech projects, not war. Who did know we will be using our passions and skills
   for that type of project? But here we did it. To help our families, to help our home,
    to make the world a better place to live. It took time since we were making the project
    while the war is going on, without many resources, kinda volunteering.
     Many thanks to the team and each individual for their work and faith.`,
  ua: ``,
};
export const FAQ_Q1 = {
  en: `Where the funds are donated?`,
  ua: `Куди донатяться кошти?`,
};
export const FAQ_A1 = {
  en: `The funds from purchasing NFT are donated to Unchain Fund (https://unchain.fund/).
   Unchain Fund is known as the most effective and transparent crypto charity project for and from Ukraine.
    That’s why Vitalik Buterin has donated 750 ETH there. Unchain Fund also has officially approved and supported our collaboration.`,
  ua: ``,
};
export const FAQ_Q2 = {
  en: `How the funds are donated?`,
  ua: `Як відбувається процес донату?`,
};
export const FAQ_A2 = {
  en: `The funds are automatically transferred from our smart contract to Unchain Fund ETH address (0x10E1439455BD2624878b243819E31CfEE9eb721C).
   Nobody, including us, cannot prevent it, since the function is hardcoded and open-sourced.
   You can check the code of our smart contract at Etherscan (https://etherscan.io/address/0x8feb5e5ac8802b772deb999eea4557c807f58bda#code),
    function donate().`,
  ua: ``,
};
export const FAQ_Q3 = {
  en: `Is the project open-sourced?`,
  ua: `Чи відкритий код проекту?`,
};
export const FAQ_A3 = {
  en: `Yes, let’s DAO it! The sources of this web app and the NFT smart contract itself are stored on GitLab 
  (https://gitlab.com/SaveUkrainianFolk). Etherscan verification of the contract’s code is also passed.`,
  ua: ``,
};
export const FAQ_Q4 = {
  en: `How to purchase the NFT?`,
  ua: `Як придбати НФТ?`,
};
export const FAQ_A4 = {
  en: `You are purchasing NFT by minting. Please, go to the Mint section above, connect your MetaMask wallet,
   choose the quantity of FOLKs you want to purchase (or the amount you want to donate) and click “Mint” button.
    You need to have enough ETH on your balance (0.1 ETH per FOLK and about 0.01 for the transaction fee).
     To deposit your Metamask simply use any crypto exchange to swap fiat into ETH, then send it to your Metamask address.
      If you do not have Metamask yet, download it first (as a browser extension on desktop, and mobile app on mobile devices). `,
  ua: ``,
};
export const FAQ_Q5 = {
  en: `Will be there any specials or airdrops for members?`,
  ua: ``,
};
export const FAQ_A5 = {
  en: `Ukrainians are a talented and grateful nation. We will see =)`,
  ua: ``,
};
export const MORE_IN_OUR_TWITTER_BUTTON_TEXT = {
  en: `More in our Twitter`,
  ua: `Більше в нашому Твіттері`,
};
export const FOOTER_TITLE_TEXT = {
  en: `From Ukraine with Love`,
  ua: ``,
};
