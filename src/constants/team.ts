import { Max, Petro, Marta, Vlad, Dima, Dasha, Elina } from '../assets/images';

export const TEAM_MEMBERS = [
  {
    name: 'Max',
    surname: 'Schnaider',
    imgSrc: Max,
    position: 'PM & Web3',
    twitter: 'https://twitter.com/MaxSchnaider',
    active: true,
  },
  {
    name: 'Petro',
    surname: 'Savchuk',
    imgSrc: Petro,
    position: 'Designer & Musician',
    instagram: 'https://www.instagram.com/sunn_halo/',
    active: true,
  },
  {
    name: 'Vlad',
    surname: 'Velitskyi',
    imgSrc: Vlad,
    position: 'Frontend Dev',
    instagram: 'https://www.instagram.com/vlad.velytskyi/',
  },
  {
    name: 'Marta',
    surname: 'Panchuk',
    imgSrc: Marta,
    position: 'Illustrator',
    instagram: 'https://www.instagram.com/marta.panchuuk/',
  },
  {
    name: 'Dasha',
    surname: '',
    imgSrc: Dasha,
    position: 'Financial support',
    instagram: 'https://www.instagram.com/da.yerofieieva/',
  },
  {
    name: 'Dima',
    surname: '',
    imgSrc: Dima,
    position: 'Video Editor',
    instagram: 'https://www.instagram.com/viunnyk/',
  },
  {
    name: 'Elina',
    surname: '',
    imgSrc: Elina,
    position: 'Video Editor',
    instagram: 'https://www.instagram.com/elina.tslk/',
  },
];
