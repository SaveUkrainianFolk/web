export const links = {
  twitter: 'https://twitter.com/SaveUAFolk',
  instagram: 'https://www.instagram.com/saveukrainianfolk/',
  gitlab: 'https://gitlab.com/SaveUkrainianFolk',
  opensea: 'https://opensea.io/collection/saveukrainianfolk',
  mangataaqiuvera: 'https://soundcloud.com/mangataaqiuvera',
  etherscan: 'https://etherscan.io/address/',
};
