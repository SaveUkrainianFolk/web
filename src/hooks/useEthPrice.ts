import { useQuery } from 'react-query';
import axios from 'axios';

const coingeckoApiBaseUri = 'https://api.coingecko.com/api/v3';

interface CoingeckoEthPriceResponse {
  ethereum: {
    usd: number;
  };
}

export const useEthPrice = () => {
  return useQuery(['get-eth-price'], async () => {
    const requestUri = `${coingeckoApiBaseUri}/simple/price?ids=ethereum&vs_currencies=usd`;
    const response = await axios.get<CoingeckoEthPriceResponse>(requestUri);
    return response.data.ethereum.usd;
  });
};
