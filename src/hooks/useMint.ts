import { useQuery } from 'react-query';
import { utils } from 'ethers';
import { useNftContract } from '.';

export interface UseMintParams {
  quantity: number;
  price: number;
}

export const useMint = ({ quantity, price }: UseMintParams) => {
  const nftContract = useNftContract();

  const { data, error, isRefetching, isLoading, refetch } = useQuery(
    ['mint', quantity, price],
    async () => {
      const tx = await nftContract?.mint(quantity, {
        value: utils.parseEther(price.toString()),
      });
      const receipt = await tx?.wait();

      let nftId: string | undefined;
      if (receipt && receipt.status === 1 && receipt.events?.length) {
        const event = receipt.events[receipt.events.length - 1];
        const nftIdBN = event.args
          ? event.args[event.args.length - 1]
          : undefined;
        nftId = utils.formatUnits(nftIdBN, 0);
      }

      return {
        tx: tx,
        receipt: receipt,
        nftId: nftId,
      } as const;
    },
    {
      enabled: false,
      cacheTime: 0,
      retry: 0,
    }
  );

  return {
    mintData: data,
    isMintLoading: isRefetching || isLoading,
    mintError: error,
    mint: refetch,
  } as const;
};
