import { useQuery } from 'react-query';
import { utils } from 'ethers';
import { useNftContract, useEthPrice } from '.';

export interface SaleInfo {
  totalSupply: number;
  maxFolks: number;
  maxFolksPerUser: number;
  price: number;
  totalDonated: number;
  totalDonatedUSD: number;
  backers: string[];
}

export const useSaleInfo = () => {
  const nftContract = useNftContract();
  const { data: ethPrice } = useEthPrice();

  const { data, refetch } = useQuery(
    ['get-sale-info'],
    async () => {
      const totalSupplyBN = await nftContract!.totalSupply();
      const totalSupply = parseInt(utils.formatUnits(totalSupplyBN, 0));

      const maxFolksBN = await nftContract!.MAX_FOLKS();
      const maxFolks = parseInt(utils.formatUnits(maxFolksBN, 0));

      const maxFolksPerUserBN = await nftContract!.MAX_FOLKS_PER_HOLDER();
      const maxFolksPerUser = parseInt(utils.formatUnits(maxFolksPerUserBN, 0));

      const priceBN = await nftContract!.PRICE_PER_FOLK();
      const price = parseFloat(utils.formatEther(priceBN));

      const totalDonated = parseFloat((price * totalSupply).toFixed(1));
      const totalDonatedUSD = Math.floor(totalDonated * ethPrice!);

      const backers: string[] = ['0xb1D7daD6baEF98df97bD2d3Fb7540c08886e0299'];
      for (let tokenId = 0; tokenId < totalSupply; tokenId++) {
        const newBacker = (
          await nftContract!.functions.ownerOf(
            utils.parseUnits(tokenId.toString(), 0)
          )
        )[0];
        if (newBacker && !backers.some((backer) => backer === newBacker)) {
          backers.push(newBacker);
        }
      }

      return {
        totalSupply: totalSupply,
        maxFolks: maxFolks,
        maxFolksPerUser: maxFolksPerUser,
        price: price,
        totalDonated: totalDonated,
        totalDonatedUSD: totalDonatedUSD,
        backers: backers,
      } as SaleInfo;
    },
    {
      enabled: !!nftContract && !!ethPrice,
      cacheTime: 0,
      refetchOnWindowFocus: false,
    }
  );

  nftContract?.on('Transfer', (from, to, tokenId) => {
    if (tokenId) {
      refetch();
    }
  });

  return {
    ...data,
  };
};
