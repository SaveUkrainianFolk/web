import { useNetwork, useSigner } from 'wagmi';
import {
  addresses,
  alchemyProvider,
  alchemyTestnetProvider,
} from '../constants';
import { ShadowsOfForgottenAncestors__factory } from '../contracts';
import { useMemo } from 'react';

export const useNftContract = (testnet?: boolean) => {
  const { data: signer } = useSigner();
  const { activeChain } = useNetwork()

  return useMemo(() => {
    const isMainnet = activeChain?.id === 1
    const signerOrProvider = isMainnet && !!signer ?
      signer : (testnet ? alchemyTestnetProvider : alchemyProvider)
    return ShadowsOfForgottenAncestors__factory.connect(
      testnet
        ? addresses.ShadowsOfForgottenAncestorsTestnet
        : addresses.ShadowsOfForgottenAncestors,
      signerOrProvider
    );
  }, [signer, activeChain]);
};
