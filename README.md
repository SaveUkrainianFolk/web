# #SaveUkrainianFolk: Web App

The app is a website landing page with the minting feature, media content, information about project and team.

The project is bootstrapped with Vite and React.js. Deployed on Vercel.

## Installation

```bash
  git clone https://github.com/SaveUkrainianFolk/web.git
  cd web
  npm ci
```

## Running in dev mode

```bash
  npm run dev
```

## Слава Україні! 🇺🇦

рускій карабль, іди нахуй!

## 🔗 Links

[![website](https://img.shields.io/website?color=%2523FF0000&down_color=white&down_message=saveukrainianfolk.org&up_message=saveukrainianfolk.org&label=https://&logo=data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tISBGb250IEF3ZXNvbWUgUHJvIDYuMS4xIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlIChDb21tZXJjaWFsIExpY2Vuc2UpIENvcHlyaWdodCAyMDIyIEZvbnRpY29ucywgSW5jLiAtLT48cGF0aCBkPSJNMzUyIDI1NkMzNTIgMjc4LjIgMzUwLjggMjk5LjYgMzQ4LjcgMzIwSDE2My4zQzE2MS4yIDI5OS42IDE1OS4xIDI3OC4yIDE1OS4xIDI1NkMxNTkuMSAyMzMuOCAxNjEuMiAyMTIuNCAxNjMuMyAxOTJIMzQ4LjdDMzUwLjggMjEyLjQgMzUyIDIzMy44IDM1MiAyNTZ6TTUwMy45IDE5MkM1MDkuMiAyMTIuNSA1MTIgMjMzLjkgNTEyIDI1NkM1MTIgMjc4LjEgNTA5LjIgMjk5LjUgNTAzLjkgMzIwSDM4MC44QzM4Mi45IDI5OS40IDM4NCAyNzcuMSAzODQgMjU2QzM4NCAyMzQgMzgyLjkgMjEyLjYgMzgwLjggMTkySDUwMy45ek00OTMuNCAxNjBIMzc2LjdDMzY2LjcgOTYuMTQgMzQ2LjkgNDIuNjIgMzIxLjQgOC40NDJDMzk5LjggMjkuMDkgNDYzLjQgODUuOTQgNDkzLjQgMTYwek0zNDQuMyAxNjBIMTY3LjdDMTczLjggMTIzLjYgMTgzLjIgOTEuMzggMTk0LjcgNjUuMzVDMjA1LjIgNDEuNzQgMjE2LjkgMjQuNjEgMjI4LjIgMTMuODFDMjM5LjQgMy4xNzggMjQ4LjcgMCAyNTYgMEMyNjMuMyAwIDI3Mi42IDMuMTc4IDI4My44IDEzLjgxQzI5NS4xIDI0LjYxIDMwNi44IDQxLjc0IDMxNy4zIDY1LjM1QzMyOC44IDkxLjM4IDMzOC4yIDEyMy42IDM0NC4zIDE2MEgzNDQuM3pNMTguNjEgMTYwQzQ4LjU5IDg1Ljk0IDExMi4yIDI5LjA5IDE5MC42IDguNDQyQzE2NS4xIDQyLjYyIDE0NS4zIDk2LjE0IDEzNS4zIDE2MEgxOC42MXpNMTMxLjIgMTkyQzEyOS4xIDIxMi42IDEyNy4xIDIzNCAxMjcuMSAyNTZDMTI3LjEgMjc3LjEgMTI5LjEgMjk5LjQgMTMxLjIgMzIwSDguMDY1QzIuOCAyOTkuNSAwIDI3OC4xIDAgMjU2QzAgMjMzLjkgMi44IDIxMi41IDguMDY1IDE5MkgxMzEuMnpNMTk0LjcgNDQ2LjZDMTgzLjIgNDIwLjYgMTczLjggMzg4LjQgMTY3LjcgMzUySDM0NC4zQzMzOC4yIDM4OC40IDMyOC44IDQyMC42IDMxNy4zIDQ0Ni42QzMwNi44IDQ3MC4zIDI5NS4xIDQ4Ny40IDI4My44IDQ5OC4yQzI3Mi42IDUwOC44IDI2My4zIDUxMiAyNTUuMSA1MTJDMjQ4LjcgNTEyIDIzOS40IDUwOC44IDIyOC4yIDQ5OC4yQzIxNi45IDQ4Ny40IDIwNS4yIDQ3MC4zIDE5NC43IDQ0Ni42SDE5NC43ek0xOTAuNiA1MDMuNkMxMTIuMiA0ODIuOSA0OC41OSA0MjYuMSAxOC42MSAzNTJIMTM1LjNDMTQ1LjMgNDE1LjkgMTY1LjEgNDY5LjQgMTkwLjYgNTAzLjZWNTAzLjZ6TTMyMS40IDUwMy42QzM0Ni45IDQ2OS40IDM2Ni43IDQxNS45IDM3Ni43IDM1Mkg0OTMuNEM0NjMuNCA0MjYuMSAzOTkuOCA0ODIuOSAzMjEuNCA1MDMuNlY1MDMuNnoiLz48L3N2Zz4=&style=for-the-badge&url=https://saveukrainianfolk.org)](https://saveukrainianfolk.org/)

[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/SaveUAFolk)
